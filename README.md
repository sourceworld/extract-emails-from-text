# Python extract emails from text


```python

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,re

import tkinter as tk
from tkinter import filedialog
from tkinter import *
from tkinter import ttk

root = Tk()
root.filename =  filedialog.askopenfilename(title = "Select file",filetypes = (("txt","*.txt *.sql"),("all files","*.*")))
ifile = root.filename
root.withdraw()


emails = []
emailsfile = open('eEmails.txt','w', encoding='utf8')

for line in open(ifile,'r', encoding='latin-1'):
	emails.append(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", line))


for email in emails:
	if email:
		email = str('\n'.join(email))
		emailsfile.write(email+'\n')
		print (email)	
	
emailsfile.close()


os.system('pause')

```